#########
#####functions


elbowplot = function(data,clus){
  c = 1:clus
  for (i in 1:clus){
    c[i] <- sum(tidy(kmeans(data,centers = i, nstart = 20))$withinss)
  }
  plot(x = 1:length(c),y = c, type = "b")
  axis(side = 1, 1:20)
}

elbowplot(d,20)
##################################################


#################################################
###Function to plot histograms of all numeric variables in dataframe

densityplot = function(df){
  c = c()
  for ( i in seq_along(df)){
#    print(class(df[,i]))
    if (is.numeric(df[,i])){
      par(mfrow = c(1,2))
      plot(density(df[,i]) , main = paste("min=",min(df[,i]),"max=",max(df[i]),sep=" "))
      hist(df[,i], main = names(df)[i])
      print(names(df)[i])
    }
  }
}
###################################################
############################################

adlag = function(dateCol,ind,dep,lag,decay,all_corr = F){
  del3  = c();  del4 = c();  xaxis = c();  yaxis = c()
  for (i in 0:lag){
    del = c()
    ind = ind[order(dateCol)]
    dep = dep[order(dateCol)]
    del = ind[1:(NROW(ind)-i)]
    for (j in seq(0,decay,.1)){
      del2 = del
      del2 = stats::filter(del,filter = j, method = "recursive")
      corr = (cor(dep[(i+1):NROW(dep)],del2))
      del3 <- append(del3, corr)
      del4 <- append(del4, paste("Corr with lag" , i , "and adstock ", j ,"is :", corr,sep = " "))
      xaxis = append(xaxis,paste(i,j))
      yaxis = append(yaxis,corr)
    }
  }
  print(del4[which.max(del3)])
  plot(as.factor(xaxis),yaxis)
  if (all_corr)print(del4)
}

library(twitteR)
library(tm)
library(wordcloud)
setup_twitter_oauth(cons_key,cons_secret,access_token,access_secret)


tremp = function(kw,no){
      lst = list()
    for (k in kw){
        tweets = searchTwitter(k, n = no)
        
        tw <<- sapply(tweets, function(x)x$getText())
        corp = Corpus(VectorSource(tw))
        toSpace <- content_transformer(function(x, pattern) {return (gsub(pattern," ",
                                                                          x, ignore.case = T))})
        
        # Apply it for substituting the regular expression given in one of the former answers by " "
        corp<- tm_map(corp,toSpace,"[^[:graph:]]")
        corp<- tm_map(corp,toSpace,"http.* ")
        corp<- tm_map(corp,toSpace,k)
        corp<- tm_map(corp,toSpace,"&amp")
        
        
        # the tolower transformation worked!
        corp <- tm_map(corp, content_transformer(tolower))
        corp  <- tm_map(corp , removePunctuation)
        corp  <- tm_map(corp , removeWords , stopwords("english") )
        corp  <<- tm_map(corp , removeNumbers )
        a.dtm1 <- TermDocumentMatrix(corp, control = list(wordLengths = c(3,10))) 
        
        m <- as.matrix(a.dtm1)
        v <- sort(rowSums(m), decreasing=TRUE)
        print(paste(sum(head(v,10)) , k ))
        lst[[k]] = data.frame(size = unname(v[1:10])/sum(v) , name = names(v[1:10]), grp = k)
        
    }
    df  = lst[[1]][0,]
    
    for (i in seq_len(length(lst))){
        df <- rbind(df,lst[[i]])
    }
    
    treemap::treemap(df , index = c("grp", "name"), vSize = "size", bg.labels = 1)
    invisible (lst)
}

tremp(c("shootout"), n = 500)

#function for qqplot to check normality
#(qqnorm fucntion in R)
plot(
    (qnorm(seq(0,1,1/(length(n)))))[-1],
    sort(n, decreasing = F)
    
)

qqnorm(n)
qqline(n)



####cramers v

cramers = function(x,y){
    
   sqrt( chisq.test(x,y)$statistic/
             (length(x)*(min(length(unique(x)) , length(unique(y)))-1)) )
}



cramers(mtcars$cyl,mtcars$gear)





